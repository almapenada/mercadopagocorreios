<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CepController extends Controller
{
  public function index()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?sCepOrigem=95720000&sCepDestino=95718000&nVlPeso=1&nCdFormato=1&nVlComprimento=20&nVlAltura=20&nVlLargura=20&nCdServico=04510&StrRetorno=xml&nIndicaCalculo=3',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: ASP.NET_SessionId=pi4zdzonl4ov5qohpl43xgad'
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);

    $xml = simplexml_load_string($response);
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);

    dd($array);


    return view('cep');
  }
}
