<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MercadoPago;

class MpController extends Controller
{
    public function index()
    {
      return view('mp');
    }

    public function checkout(Request $request)
    {
      MercadoPago\SDK::setAccessToken("TEST-7838085602798898-032218-7c96b236775c0ada9312e446a8dff3a9-732588371");

      $payment = new MercadoPago\Payment();
      $payment->transaction_amount = $request->transactionAmount;
      $payment->token = $request->token;
      $payment->description = $request->description;
      $payment->installments = $request->installments;
      $payment->payment_method_id = $request->paymentMethodId;
      $payment->issuer_id = $request->issuer;

      $payer = new MercadoPago\Payer();
      $payer->email = $request->email;
      $payer->identification = array(
          "type" => $request->docType,
          "number" => $request->docNumber
      );

      $payment->payer = $payer;
  
      $payment->save();
  
      $response = array(
          'status' => $payment->status,
          'status_detail' => $payment->status_detail,
          'id' => $payment->id
      );

      dd($response);
    }
}
