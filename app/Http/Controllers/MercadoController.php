<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MercadoPago;

class MercadoController extends Controller
{
    public function index()
    {
      MercadoPago\SDK::setAccessToken('TEST-7838085602798898-032218-7c96b236775c0ada9312e446a8dff3a9-732588371');

      // Cria um objeto de preferência
      $preference = new MercadoPago\Preference();

      // Cria um item na preferência
      $item = new MercadoPago\Item();
      $item->title = 'Meu produto';
      $item->quantity = 1;
      $item->unit_price = 75.56;
      $preference->items = array($item);
      $preference->save();

      return view('index', compact('preference', 'item'));
    }

    public function checkout(Request $request)
    {

    }
}
